#!/bin/sh

FAILURE=1
SUCCESS=0
IMAGE_VERSION=$(cat VERSION)

slack_msg_header=":x: Build failed"

if [ ${BITBUCKET_EXIT_CODE} = ${SUCCESS} ]
then
    slack_msg_header=":white_check_mark: Build success"
fi

curl -X POST "${SLACK_WEBHOOK}" -d @- <<SLACK
{
  "blocks": [
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "${slack_msg_header}"
      }
    },
    {
      "type": "divider"
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": " Image ${IMAGE_VERSION} "
      }
    }
  ]
}
SLACK