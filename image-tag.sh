#!/bin/sh

case $BITBUCKET_PR_DESTINATION_BRANCH in
    release)
        IMAGE_VERSION=$( ( git tag | egrep rc-[0-9] || echo rc-0 ) \
        | sort -V -r | head -1 | awk -F 'rc-' '{ print "rc-" $2 +1}' )
        ;;
    master)
        IMAGE_VERSION=$( ( git tag | egrep v[0-9]\.[0-9]\.[0-9] || echo v1.0.-1 ) \
        | sort -V -r | head -1 | awk -F. -v OFS=. '{$NF++;print}')
        ;;
    *)
        if [ $BITBUCKET_BRANCH = 'dev' ]
        then 
            IMAGE_VERSION=dev-$(date +'%s')
        fi
        ;;
esac

if echo $BITBUCKET_TAG | egrep -q rc-[0-9] || echo $BITBUCKET_TAG | egrep -q v[0-9]\.[0-9]\.[0-9]; 
then
    IMAGE_VERSION=$BITBUCKET_TAG;
fi

echo $IMAGE_VERSION > VERSION